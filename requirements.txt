git+https://gitlab.com/xivo.solutions/xivo-dao.git
git+https://gitlab.com/xivo.solutions/xivo-lib-python.git
sqlalchemy==0.9.8  # from xivo-dao
psycopg2==2.5.4  # from xivo-dao
