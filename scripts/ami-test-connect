#!/usr/bin/python3
# Copyright 2016 Avencall
# SPDX-License-Identifier: GPL-3.0+

# You can run it as:
#
#    python3 -u ami-test-connect 1 | tee ami-test-connect.log
#
# to test the AMI connection delay every 1 second and have the result
# displayed on stdout and saved in a log file. Look up for the word
# "error" in the log file to see if there was any connection error.

import argparse
import socket
import sys
import time

TIME_FMT = '%Y-%m-%d %H:%M:%S'


def main():
    parsed_args = parse_args()

    try:
        if parsed_args.interval:
            while True:
                test_connect(parsed_args.host, parsed_args.port)
                time.sleep(parsed_args.interval)
        else:
            test_connect(parsed_args.host, parsed_args.port)
    except KeyboardInterrupt:
        pass


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', default='127.0.0.1')
    parser.add_argument('--port', default=5038, type=int)
    parser.add_argument('interval', nargs='?', type=int)
    return parser.parse_args()


def test_connect(host, port):
    # TODO we should make sure to resolv host to an IP address before testing the connection
    #      time to make sure we aren't timing the name lookup
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        t1 = time.time()
        s.connect((host, port))
        delta_ms = int((time.time() - t1) * 1000)
        print(time.strftime(TIME_FMT), delta_ms)
    except Exception as e:
        print(time.strftime(TIME_FMT), 'error:', e)
    finally:
        s.close()


if __name__ == '__main__':
    main()
